%% Generate damping terms for EoM
% The approach shown here assumes a diagonal structure of the damping
% terms, neglecting coupling and higher than second order terms.
% Furthermore, the coefficients C_u1, C_u2, C_v1, C_v2, C_w1, C_w2,
% C_p1, C_p2, C_q1, C_q2, C_r1, C_r2 are all expected to be positive.

%% Setup
% run scubo_generate_kinematics.m

%% Coefficients
syms C_u1 C_u2 C_v1 C_v2 C_w1 C_w2 C_p1 C_p2 C_q1 C_q2 C_r1 C_r2 'real';

C = [C_u1 C_u2;
     C_v1 C_v2;
     C_w1 C_w2;
     C_p1 C_p2;
     C_q1 C_q2;
     C_r1 C_r2];

%% Damping matrix

D_matrix = sym(zeros(6,6));

%% Diagonal entries

D_matrix(1,1) = C(1,1)+C(1,2)*abs(u);
D_matrix(2,2) = C(2,1)+C(2,2)*abs(v);
D_matrix(3,3) = C(3,1)+C(3,2)*abs(w);

D_matrix(4,4) = C(4,1)+C(4,2)*abs(p);
D_matrix(5,5) = C(5,1)+C(5,2)*abs(q);
D_matrix(6,6) = C(6,1)+C(6,2)*abs(r);

%% Damping terms

D = D_matrix*E_theta;

fprintf('Generating function fun_D... ');
matlabFunction(D, 'vars', {E_theta,C}, 'file', 'fun_D');
fprintf('done!\n')
disp(' ')