function [eul] = Rotm2Eul(R)
% RotmToeul: This function calculates the intrinsic ZYX sequence of euler 
% angles corresponding to a rotation matrix.

if abs(R(3,1)) ~= 1
    phiy = -asin(R(3,1));
    % phiy = pi - phiy;
    phix = atan(R(3,2)/R(3,3));
    % phix = atan2(R(3,2),R(3,3));
    phiz = atan(R(2,1)/R(1,1));
else
    phiz = 0;
    if R(3,1) == -1
        phiy = pi/2;
        phix = phiz + atan(R(1,2)/R(1,3));
    else
        phiy = -pi/2;
        phix = -phiz + atan(R(1,2)/R(1,3));
    end
end

eul = [phix, phiy, phiz]';

end

