function [point] = circle_function(u)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% [clock,radius]

%% Memory
x = u(2,1)*cos(u(1,1));
y = u(2,1)*sin(u(1,1));
z = 0;

%% Output

point = [x,y,z,0,0,0]';

end

