function [] = scubo_plot_adaptive_E_r_EC(E_r_EC_sim)
% This function the estimated coordinates of the center of mass, i.e. E_r_EC.

t_final = E_r_EC_sim.time(end);

figure('Name','Plot of E_r_EC')
plot(E_r_EC_sim.time,E_r_EC_sim.signals.values(1:end,1),'r')
hold on
plot(E_r_EC_sim.time,E_r_EC_sim.signals.values(1:end,2),'b')
hold on
plot(E_r_EC_sim.time,E_r_EC_sim.signals.values(1:end,3),'g')
grid on
legend('x_C','y_C','z_C')
xlabel('t [s]')
ylabel('Angle [�]')
axis([0 t_final -0.1 0.1])

end

