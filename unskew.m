function r = unskew(R)
% Input: the skew-symmetric matrix R=S(r) of a vector r
% Output: r

if any(R + R')
    error('Matrix is not skew-symmetric.')
end

r = [R(3,2) R(1,3) R(2,1)]';

end

