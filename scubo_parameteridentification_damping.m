%% Parameter Identification: Damping
% Important: This is a static measurement - the accelerations are assumed
% to be zero.
% Note: 's' stands for static.

%% Setup
% clc
% run scubo_launchfile

%% Generation of symbolic terms

PI.E_r_EC = sym([0; 0; 0]);
PI.E_r_EB = sym([0; 0; 0]);
PI.m = m;
PI.E_Inertia = diag([I_xx,I_yy,I_zz]);
PI.C = C;
PI.E_m_A = diag([m_Ax, m_Ay, m_Az]);
PI.E_Inertia_A = diag([I_Axx,I_Ayy,I_Azz]);
PI.g_acc = g_acc;
PI.F_B = F_B;
PI.E_r_ET = E_r_ET{1};
PI.E_e_T = E_e_T{1};
PI.F_T = F_T;

PI.M = fun_M(PI.E_r_EC,PI.m,PI.E_Inertia);
PI.b = fun_b(E_theta,PI.E_r_EC,PI.m,PI.E_Inertia);
PI.g = fun_g(phiR,PI.E_r_EC,PI.E_r_EB,PI.m,PI.g_acc,PI.F_B);
PI.D = fun_D(E_theta,PI.C);
PI.M_A = fun_M_A(PI.E_m_A,PI.E_Inertia_A);
PI.b_A = fun_b_A(E_theta,PI.E_m_A,PI.E_Inertia_A);
PI.tau_T = fun_tau_T(PI.E_r_ET,PI.E_e_T,F_T);

% PI.M
% PI.b
% PI.g
% PI.D
% PI.M_A
% PI.b_A
% PI.tau_T

%% Static terms: Damping coefficients

PI.y_s = PI.tau_T - PI.b - PI.b_A - PI.g;

PI.pi_s = [C_u1 C_u2 C_v1 C_v2 C_w1 C_w2 C_p1 C_p2 C_q1 C_q2 C_r1 C_r2]';

PI.h_s = sym(zeros(12,6));
for i=1:6
    PI.h_s(i*2-1,i) = E_theta(i);
    PI.h_s(i*2,i) = E_theta(i)*abs(E_theta(i));
end

% Validation:
% PI.h_s'*PI.pi_s

%% Initialization

% Here, the model parameters used for identification can be changed
PI_init.E_r_EC = [0; 0; 0];
PI_init.E_r_EB = [0; 0; 0];
PI_init.m = par.m;
PI_init.E_Inertia = par.E_Inertia;
PI_init.C = par.C;
PI_init.E_m_A = par.E_m_A;
PI_init.E_Inertia_A = par.E_Inertia_A;
PI_init.g_acc = par.g_acc;
PI_init.F_B = par.F_B;
PI_init.E_r_ET = par.E_r_ET;
PI_init.E_e_T = 1/sqrt(0.085^2 + 0.105^2 + 0.045^2)*[0.085;0.105;0.045];  % par.E_e_T;

PI_init.M = fun_M(PI_init.E_r_EC,PI_init.m,PI_init.E_Inertia);
PI_init.b = fun_b(E_theta,PI_init.E_r_EC,PI_init.m,PI_init.E_Inertia);
PI_init.g = fun_g(phiR,PI_init.E_r_EC,PI_init.E_r_EB,PI_init.m,PI_init.g_acc,PI_init.F_B);
PI_init.D = fun_D(E_theta,PI_init.C);
PI_init.M_A = fun_M_A(PI_init.E_m_A,PI_init.E_Inertia_A);
PI_init.b_A = fun_b_A(E_theta,PI_init.E_m_A,PI_init.E_Inertia_A);
PI_init.tau_T = fun_tau_T(PI_init.E_r_ET,PI_init.E_e_T,F_T);

PI_init.y_s = PI_init.tau_T - PI_init.b - PI_init.b_A - PI_init.g;
PI_init.pi_s = PI.pi_s;
PI_init.h_s = PI.h_s;

% Generating functions
matlabFunction(PI_init.y_s, 'vars', {E_theta,F_T}, 'file', 'fun_y_s');
matlabFunction(PI_init.h_s, 'vars', {E_theta}, 'file', 'fun_h_s');

%% Identification: Simulation with Simulink Model
% In this section, measurements can be generated using the Simulink model.
% The input signal (generalized thruster forces), can be generated with the
% signal-builder block.

% phiq_IC = [0 0 0 1 0 0 0]';
% E_theta_IC = [0 0 0 0 0 0]';
% 
% t_sample = 0.01;
% t_final = 120;
% t_sim = 0:t_sample:t_final;
% 
% sim('scubo_model',t_sim)

% scubo_plot(phi_sim,E_theta_sim,E_dtheta_sim,F_T_sim,tau_T_sim,energy_sim)

%% Identification: Evaluation
% In this section, the actual parameter identification is performed. For
% this purpose, the function scubo_parameteridentification_damping_function
% is used.

% t_eval = [29 49 69 89 109];
% t_meas = E_theta_sim.time;
% E_theta_meas = E_theta_sim.signals.values;
% F_T_meas = F_T_sim.signals.values';

t_meas = 1:20;
t_eval = t_meas;

E_theta_meas = zeros(6,20);
E_theta_meas(6,1)  = 4*pi/5.96;
E_theta_meas(6,2)  = 4*pi/5.91;
E_theta_meas(6,3)  = 4*pi/12.08;
E_theta_meas(6,4)  = 4*pi/12.88;
E_theta_meas(4,5)  = -2*pi/2.44;
E_theta_meas(4,6)  = -2*pi/2.63;
E_theta_meas(4,7)  = -2*pi/4.15;
E_theta_meas(4,8)  = -2*pi/4.33;
E_theta_meas(5,9)  = 2*pi/2.70;
E_theta_meas(5,10) = 2*pi/2.44;
E_theta_meas(5,11) = 2*pi/4.04;
E_theta_meas(5,12) = 2*pi/4.54;
E_theta_meas(1,13) = 2.4/2.25;
E_theta_meas(1,14) = 2.4/5.33;
E_theta_meas(1,15) = 2.4/7.16;
E_theta_meas(1,16) = 2.4/6.76;
E_theta_meas(2,17) = 2.4/2.75;
E_theta_meas(2,18) = 2.4/2.56;
E_theta_meas(3,19) = 2.4/2.75; % Guess
E_theta_meas(3,20) = 2.4/2.56; % Guess

E_u_T_meas = zeros(6,20);
E_u_T_meas(6,1)  = 50;
E_u_T_meas(6,2)  = 50;
E_u_T_meas(6,3)  = 30/80*50;
E_u_T_meas(6,4)  = 27/80*50;
E_u_T_meas(4,5)  = 50;
E_u_T_meas(4,6)  = 50;
E_u_T_meas(4,7)  = 50/80*50;
E_u_T_meas(4,8)  = 40/80*50;
E_u_T_meas(5,9)  = 50;
E_u_T_meas(5,10) = 50;
E_u_T_meas(5,11) = 25/80*50;
E_u_T_meas(5,12) = 30/80*50;
E_u_T_meas(1,13) = 50;
E_u_T_meas(1,14) = 50;
E_u_T_meas(1,15) = 16/80*50;
E_u_T_meas(1,16) = 16/80*50;
E_u_T_meas(2,17) = 50;
E_u_T_meas(2,18) = 50;
E_u_T_meas(3,19) = 50; % Guess
E_u_T_meas(3,20) = 50; % Guess

F_T_meas = zeros(8,20);
for i = 1:20
    F_T_meas(:,i) = E_u_T2F_T(E_u_T_meas(:,i));
end

PI_init.pi_s_LS = zeros(12,1);
for index = 1:6
    PI_init.pi_s_LS(2*index-1:2*index,1) = scubo_parameteridentification_damping_function(index,t_eval,...
        t_meas,E_theta_meas,F_T_meas);
end

PI_init.pi_s_LS