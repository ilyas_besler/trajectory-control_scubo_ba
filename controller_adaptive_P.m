function [E_u_T] = controller_adaptive_P(phiR_des,E_theta_des,E_dtheta_des,phiR,E_theta,E_dtheta)
% adaptive_controller_P

R_IE = phiR2R_IE(phiR);
R_IE_des = phiR2R_IE(phiR_des);

e_R = 1/2*unskew(R_IE_des'*R_IE - R_IE'*R_IE_des);

K_P = diag([30 40 50]); % 80 250

E_u_T_rot = -K_P*e_R;
    
E_u_T = [0; 0; 0; E_u_T_rot];

end
