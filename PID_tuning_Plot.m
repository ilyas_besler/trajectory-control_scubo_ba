%% Plots with different values

figure
subplot(3,2,1)
plot(PID_clock,x_real,PID_clock,x_tar);
grid on

subplot(3,2,2)
plot(PID_clock,I_x);
grid on

subplot(3,2,3)
plot(PID_clock,y_real,PID_clock,y_tar);
grid on

subplot(3,2,4)
plot(PID_clock,I_y);
grid on

subplot(3,2,5)
plot(PID_clock,z_real,PID_clock,z_tar);
grid on

subplot(3,2,6)
plot(PID_clock,I_z);
grid on

%% single plots

% x Values

% figure
% subplot(2,1,1)
% plot(PID_clock,x_real,PID_clock,x_tar);
% grid on
% subplot(2,2,2)
% plot(PID_clock,PID_x);
% grid on
% 
% y Values
% figure
% subplot(2,1,1)
% plot(PID_clock,y_real,PID_clock,y_tar);
% grid on
% subplot(2,2,2)
% plot(PID_clock,PID_y);
% grid on

% % z Values
% figure
% subplot(2,1,1)
% plot(PID_clock,z_real,PID_clock,z_tar);
% grid on
% subplot(2,2,2)
% plot(PID_clock,PID_z);
% grid on

