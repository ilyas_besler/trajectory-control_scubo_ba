%% Definition and calculation of parameters
% All values are saved in the struct 'par'

%% Instructions

% The values a,b,c for calculating the added mass terms have to be 
% defined in the corresponding file scubo_generate_addedmassterms.m

%% Parameters

% Physical constants
par.rho = 1000;  % [kg/m^3]
par.g_acc = 9.81;  % [m/s^2], no 'minus' here!

% General values related to Scubo
par.m = 30;  % [kg]
par.V = 0.018;  % [m^3]
par.E_r_EC = [0;0;0];  % [m]
par.E_r_EB = [0;0;0];  % [m]
par.E_r_ET = [0.265;0.14;0.08];
par.E_e_T = 1/sqrt(3)*[1;1;1];  % 1/sqrt(0.085^2 + 0.105^2 + 0.045^2)*[0.085;0.105;0.045];
par.E_Inertia = [0.3 0 0; 0 0.3 0; 0 0 0.3];  % [kg*m^2]

% Buoyancy
% par.F_B = par.g_acc*par.V*par.rho;
par.F_B = par.g_acc*par.m;
% par.F_B = 0;

% Damping coefficients (have to be positive!)
par.C = [173    -54    ;
         326    -180   ;
         139    -77    ;
         1.0537  0.0521;
         0.1665  1.1027;
         5.4910  2.3958];

% par.C = [ 0 50;
%           0 70;
%           0 70;
%          30 80;
%          30 80;
%          30 80];

% Added mass and inertia
run scubo_parameters_addedmass