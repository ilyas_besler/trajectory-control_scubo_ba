%% Run Simulation

phiR_IC = [0 0 0 1 0 0 0 1 0 0 0 1]';
E_theta_IC = [0 0 0 0 0 0]';

t_sample = 0.01;

t_final = 24-t_sample;

t_sim = 0:t_sample:t_final;

fw = ones(1,1/t_sample);
zr = zeros(1,1/t_sample);

phiR_des_simin = [zeros(3,size(t_sim,2));
                   ones(1,size(t_sim,2));
                  zeros(3,size(t_sim,2));
                   ones(1,size(t_sim,2));
                  zeros(3,size(t_sim,2));
                   ones(1,size(t_sim,2))];

E_theta_des_simin = 0*[t_sim; t_sim; t_sim; t_sim; t_sim; t_sim];

E_dtheta_des_simin = 0*[t_sim; t_sim; t_sim; t_sim; t_sim; t_sim];

%% General velocity test

E_u_T_des_simin = 50*[zr fw zr -fw zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr fw zr -fw zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr zr zr zr zr fw zr -fw zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr zr zr zr zr zr zr zr zr fw zr -fw zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr fw zr -fw zr zr zr zr;
                      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr fw zr -fw];

F_T_des = E_u_T2F_T(E_u_T_des_simin);

sim('scubo_model',t_sim,[],[t_sim;phiR_des_simin]',[t_sim;E_theta_des_simin]',[t_sim;E_dtheta_des_simin]',[t_sim;E_u_T_des_simin]')

% Plot the results of this simulation
scubo_plot(phi_sim,E_theta_sim,E_dtheta_sim,F_T_sim,tau_T_sim,energy_sim)

%% Translational velocity test

E_u_T_des_simin = 20*[zr fw zr -fw zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr fw zr -fw zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr zr zr zr zr fw zr -fw zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr];

sim('scubo_model',t_sim,[],[t_sim;phiR_des_simin]',[t_sim;E_theta_des_simin]',[t_sim;E_dtheta_des_simin]',[t_sim;E_u_T_des_simin]')

% Plot the results of this simulation
scubo_plot(phi_sim,E_theta_sim,E_dtheta_sim,F_T_sim,tau_T_sim,energy_sim)

%% Rotational velocity test

E_u_T_des_simin = 20*[zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr fw zr fw zr -fw zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr zr zr fw zr fw zr -fw zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr zr zr zr zr zr zr zr zr fw zr fw zr -fw zr zr zr zr zr zr];

sim('scubo_model',t_sim,[],[t_sim;phiR_des_simin]',[t_sim;E_theta_des_simin]',[t_sim;E_dtheta_des_simin]',[t_sim;E_u_T_des_simin]')

% Plot the results of this simulation
scubo_plot(phi_sim,E_theta_sim,E_dtheta_sim,F_T_sim,tau_T_sim,energy_sim)

%% Driving a curve

E_u_T_des_simin = 20*[zr fw fw fw fw fw fw fw fw fw fw fw fw fw fw fw fw fw fw fw fw fw zr zr;
                      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr fw fw fw fw fw fw fw fw fw fw fw fw fw fw fw fw fw fw fw fw fw zr zr];

sim('scubo_model',t_sim,[],[t_sim;phiR_des_simin]',[t_sim;E_theta_des_simin]',[t_sim;E_dtheta_des_simin]',[t_sim;E_u_T_des_simin]')

% Plot the results of this simulation
scubo_plot(phi_sim,E_theta_sim,E_dtheta_sim,F_T_sim,tau_T_sim,energy_sim)

%% Driving a curve 2

E_u_T_des_simin = 20*[zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr fw fw fw fw fw fw fw fw fw fw fw fw fw fw fw fw fw fw fw fw fw zr zr;
                      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr fw fw fw fw fw fw fw fw fw fw fw fw fw fw fw fw fw fw fw fw fw zr zr
                      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr];

sim('scubo_model',t_sim,[],[t_sim;phiR_des_simin]',[t_sim;E_theta_des_simin]',[t_sim;E_dtheta_des_simin]',[t_sim;E_u_T_des_simin]')

% Plot the results of this simulation
scubo_plot(phi_sim,E_theta_sim,E_dtheta_sim,F_T_sim,tau_T_sim,energy_sim)

%% Driving forward, yawing and driving forward again

E_u_T_des_simin = 20*[zr fw zr -fw zr zr zr zr zr zr zr fw zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr fw zr fw zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr];

sim('scubo_model',t_sim,[],[t_sim;phiR_des_simin]',[t_sim;E_theta_des_simin]',[t_sim;E_dtheta_des_simin]',[t_sim;E_u_T_des_simin]')

% Plot the results of this simulation
scubo_plot(phi_sim,E_theta_sim,E_dtheta_sim,F_T_sim,tau_T_sim,energy_sim)

%% Gravity and buoyancy test

E_u_T_des_simin = 20*[zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr];

sim('scubo_model',t_sim,[],[t_sim;phiR_des_simin]',[t_sim;E_theta_des_simin]',[t_sim;E_dtheta_des_simin]',[t_sim;E_u_T_des_simin]')

% Plot the results of this simulation
scubo_plot(phi_sim,E_theta_sim,E_dtheta_sim,F_T_sim,tau_T_sim,energy_sim)


%% Test weird behaviour

E_u_T_des_simin = 50*[zr 0.2*fw -0.2*fw zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr     zr      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr     zr      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr     zr      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr     zr      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr     zr      zr fw fw fw fw zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr];

sim('scubo_model',t_sim,[],[t_sim;phiR_des_simin]',[t_sim;E_theta_des_simin]',[t_sim;E_dtheta_des_simin]',[t_sim;E_u_T_des_simin]')

% Plot the results of this simulation
scubo_plot(phi_sim,E_theta_sim,E_dtheta_sim,F_T_sim,tau_T_sim,energy_sim)

%% Test for wrong rotation matrices

E_u_T_des_simin = 20*[zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr zr;
                      zr zr zr zr zr zr zr zr zr zr zr zr fw fw fw fw fw fw fw fw zr zr zr zr;
                      fw fw fw fw fw fw fw fw fw fw zr zr zr zr zr zr zr zr zr zr zr zr zr zr];

sim('scubo_model',t_sim,[],[t_sim;phiR_des_simin]',[t_sim;E_theta_des_simin]',[t_sim;E_dtheta_des_simin]',[t_sim;E_u_T_des_simin]')

% Plot the results of this simulation
scubo_plot(phi_sim,E_theta_sim,E_dtheta_sim,F_T_sim,tau_T_sim,energy_sim)

%% Maximal velocity tests

E_u_T_des_simin = [ 0*ones(size(t_sim));
                    0*ones(size(t_sim));
                    0*ones(size(t_sim));
                    0*ones(size(t_sim));
                    0*ones(size(t_sim));
                   50*ones(size(t_sim))];

sim('scubo_model',t_sim,[],[t_sim;phiR_des_simin]',[t_sim;E_theta_des_simin]',[t_sim;E_dtheta_des_simin]',[t_sim;E_u_T_des_simin]')

% Plot the results of this simulation
scubo_plot(phi_sim,E_theta_sim,E_dtheta_sim,F_T_sim,tau_T_sim,energy_sim)