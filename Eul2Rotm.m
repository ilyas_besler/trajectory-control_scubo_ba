function [R] = Eul2Rotm(eul)
% EulToRotm: This function calculates the rotation matrix corresponding to
% an intrinsic ZYX sequence of euler angles.

R_x = [1 0 0; 0 cos(eul(1)) -sin(eul(1)); 0 sin(eul(1)) cos(eul(1))];
R_y = [cos(eul(2)) 0 sin(eul(2)); 0 1 0; -sin(eul(2)) 0 cos(eul(2))];
R_z = [cos(eul(3)) -sin(eul(3)) 0; sin(eul(3)) cos(eul(3)) 0; 0 0 1];

R = R_z*R_y*R_x;

end