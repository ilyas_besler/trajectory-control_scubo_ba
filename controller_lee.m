%% Leed Controller
% Note: The variable bool can take the values 0 and 1. A value of 1
% indicates that in this moment, Scubo is controlled by the spacemouse and
% that terms related to velocity and acceleration should not be activated.

%% Definition of symbolic variables
syms x_des y_des z_des r11_des r21_des r31_des r12_des r22_des r32_des r13_des r23_des r33_des 'real';
syms u_des v_des w_des p_des q_des r_des 'real';
syms du_des dv_des dw_des dp_des dq_des dr_des 'real'
syms bool 'real';

phiR_des = [x_des y_des z_des r11_des r21_des r31_des r12_des r22_des r32_des r13_des r23_des r33_des]';
E_theta_des = [u_des v_des w_des p_des q_des r_des]';
E_dtheta_des = [du_des dv_des dw_des dp_des dq_des dr_des]';

E_omega_IE_des = E_theta_des(4:6);
E_domega_IE_des = E_dtheta_des(4:6);

R_IE_des = phiR2R_IE(phiR_des);

%% Error Terms

e_R = 1/2*unskew(R_IE_des'*R_IE - R_IE'*R_IE_des);

% e_R = asin(norm(e_R))/norm(e_R+0.0000001)*e_R;

e_omega = E_omega_IE - R_IE'*R_IE_des*E_omega_IE_des;

%% Gains

% Thruster gains (needed due to the geometrical arrangement of the thrusters)
% K_T = diag([1;1;1]./([zeros(3,3),eye(3)]*fun_FTtotauT(tauTtoFT([0 0 0 1 1 1]'))));
K_T = eye(3);

k_R = 250; % 80 70 8

k_omega = 20; % 2.5

k_I = 0; % 120;

%% Controller

% tau_controller = - k_R*e_R - (1-bool)*(k_omega*e_omega + cross(E_omega_IE,(par.E_Inertia+par.E_Inertia_A)*E_omega_IE)...
%                  - (par.E_Inertia+par.E_Inertia_A)*(skewMatrix(E_omega_IE)*R_IE'*R_IE_des*E_omega_IE_des - R_IE'*R_IE_des*E_domega_IE_des));

syms e_I1 e_I2 e_I3 'real';

e_I = [e_I1 e_I2 e_I3]';

% tau_controller = - k_R*e_R - k_omega*e_omega - k_I*Pi + cross(E_omega_IE,(par.E_Inertia + par.E_Inertia_A)*E_omega_IE)...
%                  - (par.E_Inertia + par.E_Inertia_A)*(skewMatrix(E_omega_IE)*R_IE'*R_IE_des*E_omega_IE_des - R_IE'*R_IE_des*E_domega_IE_des);

E_u_T_controller_rot = K_T*(- k_R*e_R - k_omega*e_omega) - k_I*e_I;

E_u_T_controller = [0; 0; 0; E_u_T_controller_rot];

de_I = e_R;
% dPi = sym([1 1 1]');

Pi_IC = [0 0 0]';

fprintf('Generating  functions fun_controller_lee_E_u_T, fun_controller_lee_de_I... ');
matlabFunction(E_u_T_controller, 'vars', {phiR_des,E_theta_des,E_dtheta_des,phiR,E_theta,E_dtheta,e_I}, 'file', 'fun_controller_lee_E_u_T');
matlabFunction(de_I, 'vars', {phiR_des,E_theta_des,E_dtheta_des,phiR,E_theta,E_dtheta}, 'file', 'fun_controller_lee_de_I');
fprintf('done!\n')
disp(' ')