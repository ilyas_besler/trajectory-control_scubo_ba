%% Added mass and inertia coefficients for an ellipsoid with half-axes a>b>c
% These calculations are based on the formulas found in the following
% book on page 81:

% A.I. Korotkin, Added Masses of Ship Structures, 
% � Springer Science + Business Media B.V. 2009

%% INSTRUCTIONS
% The files scubo_generate_kinematics.m, scubo_generate_EoM.m and
% scubo_parameters.m must be run a priori.

%% Define parameters

% lenght of half-axes [m]
A_a = 0.6;
A_b = 0.4;
A_c = 0.3;

%% Calculations

A_0_fun = @(u) A_a.*A_b.*A_c./((A_a.^2+u).*sqrt((A_a.^2+u).*(A_b.^2+u).*(A_c.^2+u))); % Note: Without the ".", the integral does not work (for whatever reason). 
B_0_fun = @(u) A_a.*A_b.*A_c./((A_b.^2+u).*sqrt((A_a.^2+u).*(A_b.^2+u).*(A_c.^2+u)));
C_0_fun = @(u) A_a.*A_b.*A_c./((A_c.^2+u).*sqrt((A_a.^2+u).*(A_b.^2+u).*(A_c.^2+u)));

A_0 = integral(A_0_fun,0,inf);
B_0 = integral(B_0_fun,0,inf);
C_0 = integral(C_0_fun,0,inf);

m_A11 = 4/3*par.rho*A_a*A_b*A_c*A_0/(2-A_0);
m_A22 = 4/3*par.rho*A_a*A_b*A_c*B_0/(2-B_0);
m_A33 = 4/3*par.rho*A_a*A_b*A_c*C_0/(2-C_0);

I_A_11 = 4/15*pi*par.rho*A_a*A_b*A_c*(A_b^2-A_c^2)^2*(C_0-B_0)/(2*(A_b^2-A_c^2)+(B_0-C_0)*(A_b^2+A_c^2));
I_A_22 = 4/15*pi*par.rho*A_a*A_b*A_c*(A_a^2-A_c^2)^2*(A_0-C_0)/(2*(A_c^2-A_a^2)+(C_0-A_0)*(A_c^2+A_a^2));
I_A_33 = 4/15*pi*par.rho*A_a*A_b*A_c*(A_a^2-A_b^2)^2*(B_0-A_0)/(2*(A_a^2-A_b^2)+(A_0-B_0)*(A_a^2+A_b^2));

% Added mass and inertia coefficients expressed in the E-frame
par.E_m_A = diag([m_A11, m_A22, m_A33]);

par.E_Inertia_A = diag([I_A_11,I_A_22,I_A_33]);