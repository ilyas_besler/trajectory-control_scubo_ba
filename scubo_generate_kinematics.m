%% Generate kinematics

%% Parametrization: Position and orientation of the E-frame in the I-frame

syms x y z r11 r21 r31 r12 r22 r32 r13 r23 r33 'real';

phiR = [x y z r11 r21 r31 r12 r22 r32 r13 r23 r33]';

I_r_IE = [x y z]';

initial = [0 0 0 0 0 0 0]';
sphere = [10 10 10]';
%% "Generalized" velocities and accelerations expressed in the E-frame

syms u v w p q r 'real';

E_v_IE = [u v w]';
E_omega_IE = [p q r]';
E_theta = [E_v_IE; E_omega_IE];

syms du dv dw dp dq dr 'real';

E_dtheta = [du dv dw dp dq dr]';

%% Center of mass expressed in the E-frame
syms x_C y_C z_C 'real';

E_r_EC = [x_C; y_C; z_C];

%% Rotation maxtrix
R_IE = [phiR(4:6),phiR(7:9),phiR(10:12)];

%% Jacobian relating E_theta to dphiR

%original dR_IE = skewMatrix(E_omega_IE)*R_IE;
dR_IE = R_IE*skewMatrix(E_omega_IE);

dR_IE_vec = [dR_IE(:,1); dR_IE(:,2); dR_IE(:,3)];

Xi_R = equationsToMatrix(dR_IE_vec,E_omega_IE);

IE_J_phiR = [R_IE sym(zeros(3,3));sym(zeros(9,3)) Xi_R];

disp(' ')
fprintf('Generating function fun_IE_J_phiR ... ');
matlabFunction(IE_J_phiR, 'vars', {phiR}, 'file', 'fun_IE_J_phiR');
fprintf('done!\n')

%% Jacobians: CM-frame to E-frame expressed in I-frame/E-frame

EE_Jp_CE = [sym(eye(3)) -skewMatrix(E_r_EC)];
EE_Jr_CE = [sym(zeros(3,3)) sym(eye(3))];

IE_Jp_CE = [R_IE -R_IE*skewMatrix(E_r_EC)];
IE_Jr_CE = [sym(zeros(3,3)) R_IE];

% Time derivatives:

EE_dJp_CE =[skewMatrix(E_omega_IE) -skewMatrix(cross(E_omega_IE,E_r_EC))];
EE_dJr_CE =[sym(zeros(3,3)) skewMatrix(E_omega_IE)];

%% Jacobians: P-frame (random point) to E-frame
% - II_Jp_PE is needed to calculate the generalized force tau from
%   forces expressed in the I-frame (such as buoyancy, but not thrusters).
% - EI_Jp_PE is needed to calculate the generalized force tau from
%   forces expressed in the E-frame (such as thrusters, but not gravity).

% E_r_P: coordinates of a random point expressed in the E-frame 
syms x_P y_P z_P 'real';

E_r_EP = [x_P; y_P; z_P];

IE_Jp_PE = [R_IE -R_IE*skewMatrix(E_r_EP)];

EE_Jp_PE = [eye(3) -skewMatrix(E_r_EP)];
EE_Jr_PE = [sym(zeros(3,3)) sym(eye(3))];

% Time derivatives:

EE_dJp_PE =[skewMatrix(E_omega_IE) -skewMatrix(cross(E_omega_IE,E_r_EP))];
EE_dJr_PE =[sym(zeros(3,3)) skewMatrix(E_omega_IE)];

disp(' ')
fprintf('Generating jacobian functions fun_IE_Jp_PE, fun_EE_Jp_PE,\n');
fprintf('fun_EE_Jp_PE, fun_EE_Jr_PE, fun_EE_dJp_PE and fun_EE_dJr_PE ... ');
matlabFunction(IE_Jp_PE, 'vars', {phiR,E_r_EP}, 'file', 'fun_IE_Jp_PE');
matlabFunction(EE_Jp_PE, 'vars', {E_r_EP}, 'file', 'fun_EE_Jp_PE');
matlabFunction(EE_Jr_PE, 'file', 'fun_EE_Jr_PE');
matlabFunction(EE_dJp_PE, 'vars', {E_theta,E_r_EP}, 'file', 'fun_EE_dJp_PE');
matlabFunction(EE_dJr_PE, 'vars', {E_theta}, 'file', 'fun_EE_dJr_PE');
fprintf('done!\n')

%%
disp(' ')