function phi = phiR2phi(phiR)
% Orientation: This function transforms Rotation matrices to Euler angles.
% Position: Remains unchanged

% [phix, phiy, phiz] = dcm2angle(phiRToRIE(phiR)','XYZ');
% eul = rotm2eul(phiRToRIE(phiR)','ZYX');

eul = Rotm2Eul([phiR(4:6) phiR(7:9) phiR(10:12)]);

phi = [phiR(1:3); eul];

end

