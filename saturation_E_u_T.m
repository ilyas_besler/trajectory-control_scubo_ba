function [E_u_T_sat] = saturation_E_u_T(E_u_T)
% This function saturates the generalized thurster forces such that the
% relative values of the forces remain unchanged.

F_T = E_u_T2F_T(E_u_T);

F_max = abs(max(F_T));

if F_max > 50
    E_u_T_sat = 48*E_u_T/F_max;
else
    E_u_T_sat = E_u_T;
end

end

