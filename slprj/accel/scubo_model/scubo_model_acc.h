#include "__cf_scubo_model.h"
#ifndef RTW_HEADER_scubo_model_acc_h_
#define RTW_HEADER_scubo_model_acc_h_
#include <stddef.h>
#ifndef scubo_model_acc_COMMON_INCLUDES_
#define scubo_model_acc_COMMON_INCLUDES_
#include <stdlib.h>
#define S_FUNCTION_NAME simulink_only_sfcn 
#define S_FUNCTION_LEVEL 2
#define RTW_GENERATED_S_FUNCTION
#include "rtwtypes.h"
#include "simstruc.h"
#include "fixedpoint.h"
#endif
#include "scubo_model_acc_types.h"
#include "multiword_types.h"
#include "mwmathutil.h"
#include "rt_defines.h"
typedef struct { real_T j1tq2ahabw [ 6 ] ; real_T cmifhkyl1l [ 7 ] ; real_T
gdkxw1ahtu [ 7 ] ; real_T f2q5d5ggs1 ; real_T hzcsqcos3l ; real_T ka51im1c04
; real_T josvgw3kta [ 3 ] ; real_T b5adiixb2q [ 42 ] ; real_T cyqoe3yq2b [ 6
] ; real_T ngqt51xzbl [ 6 ] ; real_T mxuchyw01i [ 8 ] ; } ix4rrurb2n ;
typedef struct { void * bqhgiiiyf2 ; void * mobxfb10lr ; void * dpmf1i0aue [
3 ] ; void * mtbrn1h3vd ; void * l514yhaiiy ; void * htkcrj4l5n ; void *
fygq4h4ejk ; int_T ncafnyyfbr [ 13 ] ; int_T otyirfckw1 ; int_T llekmlow2p ;
char pad_llekmlow2p [ 4 ] ; } owomqnqk1b ; typedef struct { real_T chjotkk1ff
[ 7 ] ; real_T jpcf1riosv [ 6 ] ; } o531tj1wlx ; typedef struct { real_T
chjotkk1ff [ 7 ] ; real_T jpcf1riosv [ 6 ] ; } pue3ut3dm1 ; typedef struct {
boolean_T chjotkk1ff [ 7 ] ; boolean_T jpcf1riosv [ 6 ] ; } mxvbgs4k11 ;
typedef struct { real_T chjotkk1ff [ 7 ] ; real_T jpcf1riosv [ 6 ] ; }
phbbwcobea ; typedef struct { real_T oh4gwjcyfp ; } ix3n5dn3px ; typedef
struct { ZCSigState lqpbbkqifc ; } mgm3hr51j2 ; typedef struct { real_T
enbqmbcsoq [ 8 ] ; } l0bcmfl31z ; struct czb24kznjr_ { real_T P_0 [ 7 ] ;
real_T P_1 [ 6 ] ; real_T P_2 ; uint8_T P_3 ; uint8_T P_4 ; char pad_P_4 [ 6
] ; } ; extern czb24kznjr ewfkoj5xaa ;
#endif
