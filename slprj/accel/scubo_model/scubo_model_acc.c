#include "__cf_scubo_model.h"
#include <math.h>
#include "scubo_model_acc.h"
#include "scubo_model_acc_private.h"
#include <stdio.h>
#include "slexec_vm_simstruct_bridge.h"
#include "slexec_vm_zc_functions.h"
#include "simstruc.h"
#include "fixedpoint.h"
#define CodeFormat S-Function
#define AccDefine1 Accelerator_S-Function
static void mdlOutputs ( SimStruct * S , int_T tid ) { int32_T i ; int32_T
i_p ; ix4rrurb2n * _rtB ; czb24kznjr * _rtP ; o531tj1wlx * _rtX ; owomqnqk1b
* _rtDW ; _rtDW = ( ( owomqnqk1b * ) ssGetRootDWork ( S ) ) ; _rtX = ( (
o531tj1wlx * ) ssGetContStates ( S ) ) ; _rtP = ( ( czb24kznjr * )
ssGetDefaultParam ( S ) ) ; _rtB = ( ( ix4rrurb2n * ) _ssGetBlockIO ( S ) ) ;
for ( i = 0 ; i < 7 ; i ++ ) { _rtB -> gdkxw1ahtu [ i ] = _rtX -> chjotkk1ff
[ i ] ; } ssCallAccelRunBlock ( S , 0 , 1 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 0 , 2 , SS_CALL_MDL_OUTPUTS ) ; for ( i = 0 ; i < 6
; i ++ ) { _rtB -> cyqoe3yq2b [ i ] = _rtX -> jpcf1riosv [ i ] ; }
ssCallAccelRunBlock ( S , 0 , 4 , SS_CALL_MDL_OUTPUTS ) ; ssCallAccelRunBlock
( S , 0 , 5 , SS_CALL_MDL_OUTPUTS ) ; for ( i = 0 ; i < 6 ; i ++ ) { _rtB ->
ngqt51xzbl [ i ] *= _rtP -> P_2 ; } ssCallAccelRunBlock ( S , 0 , 7 ,
SS_CALL_MDL_OUTPUTS ) ; ssCallAccelRunBlock ( S , 0 , 8 , SS_CALL_MDL_OUTPUTS
) ; ssCallAccelRunBlock ( S , 0 , 9 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 0 , 10 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 0 , 11 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 0 , 12 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 0 , 13 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 0 , 14 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 0 , 15 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 0 , 16 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 0 , 17 , SS_CALL_MDL_OUTPUTS ) ; _rtB -> josvgw3kta
[ 0 ] = _rtB -> f2q5d5ggs1 ; _rtB -> josvgw3kta [ 1 ] = _rtB -> hzcsqcos3l ;
_rtB -> josvgw3kta [ 2 ] = _rtB -> ka51im1c04 ; ssCallAccelRunBlock ( S , 0 ,
19 , SS_CALL_MDL_OUTPUTS ) ; ssCallAccelRunBlock ( S , 0 , 20 ,
SS_CALL_MDL_OUTPUTS ) ; for ( i = 0 ; i < 7 ; i ++ ) { _rtB -> cmifhkyl1l [ i
] = 0.0 ; for ( i_p = 0 ; i_p < 6 ; i_p ++ ) { _rtB -> cmifhkyl1l [ i ] +=
_rtB -> b5adiixb2q [ 7 * i_p + i ] * _rtB -> cyqoe3yq2b [ i_p ] ; } }
UNUSED_PARAMETER ( tid ) ; }
#define MDL_UPDATE
static void mdlUpdate ( SimStruct * S , int_T tid ) { UNUSED_PARAMETER ( tid
) ; }
#define MDL_DERIVATIVES
static void mdlDerivatives ( SimStruct * S ) { int32_T i ; ix4rrurb2n * _rtB
; pue3ut3dm1 * _rtXdot ; _rtXdot = ( ( pue3ut3dm1 * ) ssGetdX ( S ) ) ; _rtB
= ( ( ix4rrurb2n * ) _ssGetBlockIO ( S ) ) ; for ( i = 0 ; i < 7 ; i ++ ) {
_rtXdot -> chjotkk1ff [ i ] = _rtB -> cmifhkyl1l [ i ] ; } for ( i = 0 ; i <
6 ; i ++ ) { _rtXdot -> jpcf1riosv [ i ] = _rtB -> j1tq2ahabw [ i ] ; } }
#define MDL_ZERO_CROSSINGS
static void mdlZeroCrossings ( SimStruct * S ) { ix4rrurb2n * _rtB ;
owomqnqk1b * _rtDW ; _rtDW = ( ( owomqnqk1b * ) ssGetRootDWork ( S ) ) ; _rtB
= ( ( ix4rrurb2n * ) _ssGetBlockIO ( S ) ) ; ssCallAccelRunBlock ( S , 0 , 5
, SS_CALL_MDL_ZERO_CROSSINGS ) ; } static void mdlInitializeSizes ( SimStruct
* S ) { ssSetChecksumVal ( S , 0 , 3284672573U ) ; ssSetChecksumVal ( S , 1 ,
2375984579U ) ; ssSetChecksumVal ( S , 2 , 2881595980U ) ; ssSetChecksumVal (
S , 3 , 2984398128U ) ; { mxArray * slVerStructMat = NULL ; mxArray *
slStrMat = mxCreateString ( "simulink" ) ; char slVerChar [ 10 ] ; int status
= mexCallMATLAB ( 1 , & slVerStructMat , 1 , & slStrMat , "ver" ) ; if (
status == 0 ) { mxArray * slVerMat = mxGetField ( slVerStructMat , 0 ,
"Version" ) ; if ( slVerMat == NULL ) { status = 1 ; } else { status =
mxGetString ( slVerMat , slVerChar , 10 ) ; } } mxDestroyArray ( slStrMat ) ;
mxDestroyArray ( slVerStructMat ) ; if ( ( status == 1 ) || ( strcmp (
slVerChar , "8.6" ) != 0 ) ) { return ; } } ssSetOptions ( S ,
SS_OPTION_EXCEPTION_FREE_CODE ) ; if ( ssGetSizeofDWork ( S ) != sizeof (
owomqnqk1b ) ) { ssSetErrorStatus ( S ,
"Unexpected error: Internal DWork sizes do "
"not match for accelerator mex file." ) ; } if ( ssGetSizeofGlobalBlockIO ( S
) != sizeof ( ix4rrurb2n ) ) { ssSetErrorStatus ( S ,
"Unexpected error: Internal BlockIO sizes do "
"not match for accelerator mex file." ) ; } if ( ssGetSizeofU ( S ) != sizeof
( l0bcmfl31z ) ) { static char msg [ 256 ] ; sprintf ( msg ,
"Unexpected error: Internal ExternalInputs sizes do "
"not match for accelerator mex file." ) ; ssSetErrorStatus ( S , msg ) ; } {
int ssSizeofParams ; ssGetSizeofParams ( S , & ssSizeofParams ) ; if (
ssSizeofParams != sizeof ( czb24kznjr ) ) { static char msg [ 256 ] ; sprintf
( msg , "Unexpected error: Internal Parameters sizes do "
"not match for accelerator mex file." ) ; } } _ssSetDefaultParam ( S , (
real_T * ) & ewfkoj5xaa ) ; } static void mdlInitializeSampleTimes (
SimStruct * S ) { } static void mdlTerminate ( SimStruct * S ) { }
#include "simulink.c"
