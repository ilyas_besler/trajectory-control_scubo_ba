function [] = scubo_plot_controller_test(phi_sim,phi_des_sim,tau_T_controller_sim)
% This function plots phi, phi_des and tau_T_controller.

t_final = phi_sim.time(end);

% Roll
figure('Name','Plots of phix and phix_des')
plot(phi_sim.time,180/pi*phi_sim.signals.values(4,1:end),'r')
grid on
hold on
plot(phi_des_sim.time,180/pi*phi_des_sim.signals.values(4,1:end),'b')
grid on
legend('phi_x','phi_x_,_d_e_s')
xlabel('t [s]')
ylabel('Angle [�]')

%Pitch
figure('Name','Plots of phiy and phiy_des')
plot(phi_sim.time,180/pi*phi_sim.signals.values(5,1:end),'r')
grid on
hold on
plot(phi_des_sim.time,180/pi*phi_des_sim.signals.values(5,1:end),'b')
grid on
legend('phi_y','phi_y_,_d_e_s')
xlabel('t [s]')
ylabel('Angle [�]')

% Yaw
figure('Name','Plots of phiz and phiz_des')
plot(phi_sim.time,180/pi*phi_sim.signals.values(6,1:end),'r')
grid on
hold on
plot(phi_des_sim.time,180/pi*phi_des_sim.signals.values(6,1:end),'b')
grid on
legend('phi_z','phi_z_,_d_e_s')
xlabel('t [s]')
ylabel('Angle [�]')

% tau_T_controller
tau_T_max = fun_FTtotauT(tauTtoFT([50 50 50 50 50 50]'));

figure('Name','Plot of generalized thruster forces')
subplot(2,3,1)
plot(tau_T_controller_sim.time,tau_T_controller_sim.signals.values(1,1:end),'r')
hold on
plot([0,tau_T_controller_sim.time(end)],tau_T_max(1)*[1 1],'k')
xlabel('t')
ylabel('tau_T_u')
axis([0 t_final -120 120])
grid on
grid minor
subplot(2,3,2)
plot(tau_T_controller_sim.time,tau_T_controller_sim.signals.values(2,1:end),'b')
hold on
plot([0,tau_T_controller_sim.time(end)],tau_T_max(2)*[1 1],'k')
xlabel('t')
ylabel('tau_T_v')
axis([0 t_final -120 120])
grid on
grid minor
subplot(2,3,3)
plot(tau_T_controller_sim.time,tau_T_controller_sim.signals.values(3,1:end),'g')
hold on
plot([0,tau_T_controller_sim.time(end)],tau_T_max(3)*[1 1],'k')
xlabel('t')
ylabel('tau_T_w')
axis([0 t_final -120 120])
grid on
grid minor
subplot(2,3,4)
plot(tau_T_controller_sim.time,tau_T_controller_sim.signals.values(4,1:end),'r')
hold on
plot([0,tau_T_controller_sim.time(end)],tau_T_max(4)*[1 1],'k')
xlabel('t')
ylabel('tau_T_p')
axis([0 t_final -8 8])
grid on
grid minor
subplot(2,3,5)
plot(tau_T_controller_sim.time,tau_T_controller_sim.signals.values(5,1:end),'b')
hold on
plot([0,tau_T_controller_sim.time(end)],tau_T_max(5)*[1 1],'k')
xlabel('t')
ylabel('tau_T_q')
axis([0 t_final -25 25])
grid on
grid minor
subplot(2,3,6)
plot(tau_T_controller_sim.time,tau_T_controller_sim.signals.values(6,1:end),'g')
hold on
plot([0,tau_T_controller_sim.time(end)],tau_T_max(6)*[1 1],'k')
xlabel('t')
ylabel('tau_T_r')
axis([0 t_final -15 15])
grid on
grid minor


end

