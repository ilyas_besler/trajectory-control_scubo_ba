%% Drag calculations

rho = 1000 % [kg/m^3]

u_measured = 17*0.4/13.6 % [m/s]
F_measured = 1.5*9.81 % [N]
c_D = 2*F_measured/(rho*u_measured^2*(0.23*0.37+0.08*0.08*4))

l_box = 0.328;
w_box = 0.33;
h_box = 0.238;

c_D = 1.3

F_u = 0.5*rho*u_measured^2*c_D*w_box*h_box
F_v = 0.5*rho*u_measured^2*c_D*l_box*h_box
F_w = 0.5*rho*u_measured^2*c_D*l_box*w_box

C_uu = 0.5*rho*c_D*w_box*h_box
C_vv = 0.5*rho*c_D*l_box*h_box
C_ww = 0.5*rho*c_D*l_box*w_box