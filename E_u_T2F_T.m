function [F_T] = E_u_T2F_T(E_u_T)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

n = size(E_u_T,2);

F_T = zeros(8,n);

M1 = [ 0  0  0  0  1  0;
       1  0  0  0  0  1;
       1  1  0  1  0  0;
       0  1  0  1  1  1;
       0  0  1  1  0  0;
       1  0  1  1  1  1;
       1  1  1  0  1  0;
       0  1  1  0  0  1];

M2 = ones(8,6) - M1;

for j = 1:n;
    for i = 1:6;
       if E_u_T(i,j)>0;
           F_T(:,j) = F_T(:,j) + M1(:,i)*E_u_T(i,j);
       end
       if E_u_T(i,j)<0;
           F_T(:,j) = F_T(:,j) - M2(:,i)*E_u_T(i,j);
       end
    end 
end

end

