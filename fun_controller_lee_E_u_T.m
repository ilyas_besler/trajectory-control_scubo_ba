function E_u_T_controller = fun_controller_lee_E_u_T(in1,in2,in3,in4,in5,in6,in7)
%FUN_CONTROLLER_LEE_E_U_T
%    E_U_T_CONTROLLER = FUN_CONTROLLER_LEE_E_U_T(IN1,IN2,IN3,IN4,IN5,IN6,IN7)

%    This function was generated by the Symbolic Math Toolbox version 6.3.
%    23-Apr-2016 16:39:57

p = in5(4,:);
p_des = in2(4,:);
q = in5(5,:);
q_des = in2(5,:);
r = in5(6,:);
r11 = in4(4,:);
r12 = in4(7,:);
r13 = in4(10,:);
r21 = in4(5,:);
r22 = in4(8,:);
r23 = in4(11,:);
r31 = in4(6,:);
r32 = in4(9,:);
r33 = in4(12,:);
r11_des = in1(4,:);
r12_des = in1(7,:);
r13_des = in1(10,:);
r21_des = in1(5,:);
r22_des = in1(8,:);
r23_des = in1(11,:);
r31_des = in1(6,:);
r32_des = in1(9,:);
r33_des = in1(12,:);
r_des = in2(6,:);
E_u_T_controller = [0.0;0.0;0.0;p.*-2.0e1-r12.*r13_des.*1.25e2+r13.*r12_des.*1.25e2-r22.*r23_des.*1.25e2+r23.*r22_des.*1.25e2-r32.*r33_des.*1.25e2+r33.*r32_des.*1.25e2+p_des.*(r11.*r11_des+r21.*r21_des+r31.*r31_des).*2.0e1+q_des.*(r11.*r12_des+r21.*r22_des+r31.*r32_des).*2.0e1+r_des.*(r11.*r13_des+r21.*r23_des+r31.*r33_des).*2.0e1;q.*-2.0e1+r11.*r13_des.*1.25e2-r13.*r11_des.*1.25e2+r21.*r23_des.*1.25e2-r23.*r21_des.*1.25e2+r31.*r33_des.*1.25e2-r33.*r31_des.*1.25e2+p_des.*(r12.*r11_des+r22.*r21_des+r32.*r31_des).*2.0e1+q_des.*(r12.*r12_des+r22.*r22_des+r32.*r32_des).*2.0e1+r_des.*(r12.*r13_des+r22.*r23_des+r32.*r33_des).*2.0e1;r.*-2.0e1-r11.*r12_des.*1.25e2+r12.*r11_des.*1.25e2-r21.*r22_des.*1.25e2+r22.*r21_des.*1.25e2-r31.*r32_des.*1.25e2+r32.*r31_des.*1.25e2+p_des.*(r13.*r11_des+r23.*r21_des+r33.*r31_des).*2.0e1+q_des.*(r13.*r12_des+r23.*r22_des+r33.*r32_des).*2.0e1+r_des.*(r13.*r13_des+r23.*r23_des+r33.*r33_des).*2.0e1];
