%% Generate equations of motion
% The EoM will have the following form: 
% (M+M_A)*E_dtheta + b + b_A + g + D = tau_T

%% Setup

% run scubo_generate_kinematics.m

%% Dynamics

syms m g_acc F_B I_xx I_yy I_zz I_xy I_xz I_yz x_B y_B z_B 'real';

% Inertia tensor of submarine with respect to E-frame
E_Inertia = [I_xx I_xy I_xz;
             I_xy I_yy I_yz;
             I_xz I_yz I_zz];

E_Inertia_C = E_Inertia - m*(E_r_EC'*E_r_EC*eye(3) - E_r_EC*E_r_EC');

% Gravity acceleration with respect to the I-frame
I_F_g = [0; 0; -m*g_acc];

% Buoyancy force expressed in the I-frame
I_F_B = [0; 0; F_B];

% Position of the center of buoyancy expressed in the E-frame
E_r_EB = [x_B; y_B; z_B];

%% Mass matrix M

M = EE_Jp_CE'*m*EE_Jp_CE + EE_Jr_CE'*E_Inertia_C*EE_Jr_CE;
M = simplify(M);

%% Gravity term g (includes buoyancy)
% Note: the Minus-signs arise, since the z-Axis of the E-frame is assumed to
% point upwards (as long as no rotation takes place).

g = -IE_Jp_CE'*I_F_g-fun_IE_Jp_PE(phiR,E_r_EB)'*I_F_B;
g = simplify(g);

%% Coriolis and Centrifugal term b

b = EE_Jp_CE'*m*EE_dJp_CE*E_theta+EE_Jr_CE'*E_Inertia_C*EE_dJr_CE*E_theta+...
    EE_Jr_CE'*cross(EE_Jr_CE*E_theta,E_Inertia_C*EE_Jr_CE*E_theta);
b = simplify(b);

%% Generate functions

fprintf('Generating  functions fun_M, fun_b, fun_g... ');
matlabFunction(M, 'vars', {E_r_EC,m,E_Inertia}, 'file', 'fun_M');
matlabFunction(b, 'vars', {E_theta,E_r_EC,m,E_Inertia}, 'file', 'fun_b');
matlabFunction(g, 'vars', {phiR,E_r_EC,E_r_EB,m,g_acc,F_B}, 'file', 'fun_g');
fprintf('done!\n')
disp(' ')
