function [output] = controller_boolean_I(phiR_des,E_theta_des,E_dtheta_des,phiR,E_theta,E_dtheta,E_u_T)
% controller_I

R_IE = phiR2R_IE(phiR);
R_IE_des = phiR2R_IE(phiR_des);
E_omega_IE = E_theta(4:6);
E_omega_IE_des = E_theta_des(4:6);

e_R = 1/2*unskew(R_IE_des'*R_IE - R_IE'*R_IE_des);

e_R_norm = norm(e_R);

e_omega = E_omega_IE - R_IE'*R_IE_des*E_omega_IE_des;

e_omega_norm = norm(e_omega);

epsilon1 = 0.2; %0.15
epsilon2 = 0.1;

K_I = 0*150; %100

bool = 0;

if max(E_u_T2F_T(E_u_T)) > 48
    dI_rot = [0 0 0]';
elseif e_R_norm < epsilon1 && -e_omega_norm < epsilon2
    dI_rot = -K_I*e_R;
else
    dI_rot = [0 0 0]';
    bool = 1;
end

dI = [0; 0; 0; dI_rot];

output = [dI; bool];

end
