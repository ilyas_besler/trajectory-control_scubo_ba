%% Generate fun_scubo_EoM
% In this file, the function fun_scubo_EoM needed for simulink models
% is generated. Therefore, first all the symbolic parameters in the
% EoM terms are initialized with their numerical counterparts.
% The terms are saved in the struct 'EoM'.

%% Setup
% run scubo_parameters.m

syms F1 F2 F3 F4 F5 F6 F7 F8 'real';
F_T = [F1 F2 F3 F4 F5 F6 F7 F8]';

%% Initializing the EoM terms with the numerical values

EoM.M     = fun_M(par.E_r_EC,par.m,par.E_Inertia);
EoM.b     = simplify(fun_b(E_theta,par.E_r_EC,par.m,par.E_Inertia));
EoM.g     = simplify(fun_g(phiR,par.E_r_EC,par.E_r_EB,par.m,par.g_acc,par.F_B));
EoM.M_A   = fun_M_A(par.E_m_A,par.E_Inertia_A);
EoM.b_A   = simplify(fun_b_A(E_theta,par.E_m_A,par.E_Inertia_A));
EoM.tau_T = simplify(fun_tau_T(par.E_r_ET,par.E_e_T,F_T));
EoM.D     = simplify(fun_D(E_theta,par.C));

%% Energy of the system

% Kinetic energy
energy_kin = 1/2*E_theta'*(EoM.M+EoM.M_A)*E_theta;

% Potential energy
energy_pot = par.m*[0 0 par.g_acc]*(R_IE*par.E_r_EC + I_r_IE)...
             - [0 0 par.F_B]*(R_IE*par.E_r_EB + I_r_IE)...
             - par.m*[0 0 par.g_acc]*(eye(3,3)*par.E_r_EC)...
             + [0 0 par.F_B]*(eye(3,3)*par.E_r_EB);

% Total energy
energy_tot = energy_kin + energy_pot;

%% Generate functions

E_dtheta_lin = linsolve(EoM.M + EoM.M_A, -(EoM.b + EoM.b_A + EoM.g + EoM.D) + EoM.tau_T);
E_dtheta_lin = simplify(E_dtheta_lin);

fprintf('Generating function fun_scubo_EoM... ')
matlabFunction(E_dtheta_lin, 'vars', {phiR,E_theta,F_T}, 'file', 'fun_scubo_EoM');
fprintf('done!\n')
disp(' ')

fprintf('Generating function fun_F_T2tau_T... ')
matlabFunction(EoM.tau_T, 'vars', {F_T}, 'file', 'fun_F_T2tau_T');
fprintf('done!\n')
disp(' ')

fprintf('Generating functions fun_energy_kin, fun_energy_pot, fun_energy_tot... ')
matlabFunction(energy_kin, 'vars', {phiR,E_theta}, 'file', 'fun_energy_kin');
matlabFunction(energy_pot, 'vars', {phiR}, 'file', 'fun_energy_pot');
matlabFunction(energy_tot, 'vars', {phiR,E_theta}, 'file', 'fun_energy_tot');
fprintf('done!\n')
disp(' ')
