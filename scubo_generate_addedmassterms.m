%% Added mass and inertia coefficients

syms m_Ax m_Ay m_Az I_Axx I_Ayy I_Azz 'real'

E_m_A = diag([m_Ax, m_Ay, m_Az]);

E_Inertia_A = diag([I_Axx,I_Ayy,I_Azz]);

%% Location of the center of added mass with respect to E
% Assumed to be at the geometrical center of Scubo anyway

E_r_EA = sym([0;0;0]);

%% Added mass matrix M_A for EoM
fprintf('Calculating added mass matrix M_A for EoM... ');
M_A = fun_EE_Jp_PE(E_r_EA)'*E_m_A*fun_EE_Jp_PE(E_r_EA) + fun_EE_Jr_PE()'*E_Inertia_A*fun_EE_Jr_PE();
M_A = simplify(M_A);
fprintf('done!\n')
disp(' ')

%% Added mass coriolis and centrifugal term b_A for EoM
fprintf('Calculating added mass coriolis and centrifugal term b_A for EoM... ');
b_A = fun_EE_Jp_PE(E_r_EA)'*E_m_A*fun_EE_dJp_PE(E_theta,E_r_EA)*E_theta + fun_EE_Jr_PE()'*E_Inertia_A*fun_EE_dJr_PE(E_theta)*E_theta +...
      fun_EE_Jr_PE()'*cross(fun_EE_Jr_PE()*E_theta,E_Inertia_A*fun_EE_Jr_PE()*E_theta);
b_A = simplify(b_A);
fprintf('done!\n')
disp(' ')

%% Generate functions

% Workaround begin
% Needed, since else Matlab will complain about invalid variable names
% when generating the functions.
% Reason: No numbers are allowed to be contained in a variable.
syms I_Axy I_Axz I_Ayz m_Axy m_Axz m_Ayz 'real';
E_Inertia_A = [I_Axx I_Axy I_Axz;
               I_Axy I_Ayy I_Ayz;
               I_Axz I_Ayz I_Azz];
E_m_A = [m_Ax  m_Axy m_Axz;
         m_Axy m_Ay  m_Ayz;
         m_Axz m_Ayz m_Az];
% Workaround end
     
fprintf('Generating functions fun_M_A, fun_b_A... ');
matlabFunction(M_A, 'vars', {E_m_A,E_Inertia_A}, 'file', 'fun_M_A');
matlabFunction(b_A, 'vars', {E_theta,E_m_A,E_Inertia_A}, 'file', 'fun_b_A');
fprintf('done!\n')
disp(' ')