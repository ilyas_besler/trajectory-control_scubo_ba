function [phiR_noroll] = phiR_noroll(phiR)
% This function sets the roll-angle of a rotation matrix to zero

r1 = phiR(4:6,1);

if norm([0 0 1]' - abs(r1)) <= 0.00001;
    phiR_noroll = phiR;  % Avoid singularities
else
    r2 = cross([0,0,1]',r1);
    r2 = r2/norm(r2);
    r3 = cross(r1,r2);
    r3 = r3/norm(r3);
    phiR_noroll = [phiR(1:3);r1;r2;r3];
end

end

