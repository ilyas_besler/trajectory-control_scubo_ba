%% Adaptive Controller: Model-based terms

%% Error-vectors
syms x_des y_des z_des r11_des r21_des r31_des r12_des r22_des r32_des r13_des r23_des r33_des 'real';
syms u_des v_des w_des p_des q_des r_des 'real';
syms du_des dv_des dw_des dp_des dq_des dr_des 'real'

phiR_des = [x_des y_des z_des r11_des r21_des r31_des r12_des r22_des r32_des r13_des r23_des r33_des]';
E_theta_des = [u_des v_des w_des p_des q_des r_des]';
E_dtheta_des = [du_des dv_des dw_des dp_des dq_des dr_des]';

% Attitude error
R_IE_des = phiR2R_IE(phiR_des);
e_r = -0.5*unskew(R_IE_des'*R_IE - R_IE'*R_IE_des); % simplify has no effect here

%% Gains

% Integrator Gain
K_Pi = 1/10000; % 1/7000

% Thruster gains: Needed due to the geometrical arrangement of the thrusters
K_T = diag([1;1;1]./([zeros(3,3),eye(3)]*fun_F_T2tau_T(E_u_T2F_T([0 0 0 1 1 1]'))));

%% Model-based terms
% Only gravity is considered: The adaptive parameters are the coordinates
% of the centre of mass w.r.t. the E-frame.

Pi = E_r_EC;

Psi = equationsToMatrix(g(4:6),Pi);
Psi = subs(Psi,phiR,phiR_des);
Psi = subs(Psi,[g_acc,m],[par.g_acc,par.m]);

dPi = K_Pi*Psi'*e_r;
% dPi = sym([0 0 0]');

E_u_T_modelbased_rot = K_T*Psi*Pi;

E_u_T_modelbased = [0; 0; 0; E_u_T_modelbased_rot];

%% Set initial conditions
Pi_IC = [0 0 0]';

fprintf('Generating  functions fun_controller_adaptive_modelterms, fun_controller_adaptive_dPi... ');
matlabFunction(E_u_T_modelbased, 'vars', {phiR_des,E_theta_des,E_dtheta_des,phiR,E_theta,E_dtheta,Pi}, 'file', 'fun_controller_adaptive_modelterms');
matlabFunction(dPi, 'vars', {phiR_des,E_theta_des,E_dtheta_des,phiR,E_theta,E_dtheta}, 'file', 'fun_controller_adaptive_dPi');
fprintf('done!\n')
disp(' ')
