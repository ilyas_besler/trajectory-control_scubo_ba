function [p_t] = get_target(u)

% u = [clock 0 0 0 0 0 0; 0 0 0 0 0 0 0;x1 y1 z1 0 0 0 t1; x2 y2 z2 0 0 0 t2; x3 y3 z3 0 0 0 t3];
% u = [7xn] array

% Test
%u = [[21 0 0 0 0 0 0]',[0 0 0 0 0 0 0]',[0 1 0 0 0 0 10; 1 1 0 0 0 0 20]']

%% Getting points

%dimension of input signal: [7,n]
dimens = size(u); 

%points of input signal go from 2 until n (out of dimens(1,2))
points = u(:,2:dimens(1,2)); %points = [7,]

%[0 0 0, 0 0 0, 0;x1 y1 z1, 0 0 0, t1; x2 y2 z2, 0 0 0, t2; ...]'; 

%% finding the correct interval in time

%reading out all times from points: last row of points
times = points(7,:); %
clock = u(1,1);


if clock < times(1,(dimens(1,2)-1))
    
    %getting the slot: left side of intervall
    slot = find(times<u(1,1),1,'last');

    %defining both sides for time -> slot & slot+1
    t_a = times(1,slot);
    t_b = times(1,slot+1);

    p = (clock-t_a)/(t_b-t_a);

    %% defining points
    a = points(1:6,slot);
    b = points(1:6,slot+1);

    if slot == dimens(1,2)-1
        p_t = points(1:6,slot+1);
    else
        p_t = a+p*(b-a);
    end

    
else 
    
    p_t = points(1:6,(dimens(1,2)-1));
    
end




end

