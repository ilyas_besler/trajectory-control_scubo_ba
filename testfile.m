%% Test-file

u_test = -3:0.1:3;

index_test = 5;

F_D_test = u_test*PI_init.pi_s_LS(2*index_test-1) + PI_init.pi_s_LS(2*index_test)*u_test.*abs(u_test);

plot(u_test,F_D_test)
grid on