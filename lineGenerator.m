function [point] = lineGenerator(u)

% [signal1, clock,counter1, counter2,0 0 0 0 0 0 0, x y z 0 0 0 t, x y z 0 0 0 t, x y z 0 0 0 t,...]

%% Memory
counter1 = u(3,1); %n�chster Wendepunkt; Start: 5
counter2 = u(4,1); %Startzeitpunkt; Start: 11

%% Linie

%% p part

% %p = [(clock-t_a)/(t_b-t_a)];

t_a = u(counter2,1);
t_b = u(counter2+7,1);

p = (u(2,1)-t_a)/(t_b-t_a);


    if u(1,1) == 0
        a = u(counter1:counter1+5,1);
        b = u(counter1+7:counter1+12,1);
        point = [a+p*(b-a);counter1;counter2];
    else  
        if ((counter1 + 19) < length(u)) && ((counter2 + 14) < length(u))
            counter1 = counter1+7;
            counter2 = counter2+7;
        end
        a = u(counter1:counter1+5,1);
        b = u(counter1+7:counter1+12,1);
        point = [a+p*(b-a);counter1;counter2];
    end



end

