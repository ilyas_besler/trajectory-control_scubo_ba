function [point] = pid_tuning(u)

%u=[clock 0 0 0 0 0 0;0 0 0 0 0 0 0; x1 y1 z1 0 0 0 t1]'

p = (u(1,1))/(u(7,3));

point = p*u(1:6,3);

end

