function [ point ] = pointSelection_linegenerator(u)

% [signal1, counter, x y z 0 0 0 t, x y z 0 0 0 t,...]

counter = u(2,1);

if u(1,1) == 0
    point = [u(counter:counter+5,1);counter];
    
else 
    if ((counter + 12) < length(u))
        counter = counter+7;
    end
    point = [u(counter:counter+5,1);counter];
end     
    
    
end

