function S = skewMatrix(x)
  % Input: a vector in R3
  % Output: the skew matrix S(x)
  S = [0    -x(3)  x(2);
  	   x(3)  0    -x(1);
      -x(2)  x(1)  0];
end