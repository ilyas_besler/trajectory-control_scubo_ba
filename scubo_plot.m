function [] = scubo_plot(phi_sim,E_theta_sim,E_dtheta_sim,F_T_sim,tau_T_sim,energy_sim)
% This function plots phi, E_theta, E_dtheta, tau_T, F_T and the Energy.

t_final = phi_sim.time(end);

%% x, y, z and derivatives

figure('Name','Plots of x, y, z, and derivatives')
subplot(3,3,1)
plot(phi_sim.time,phi_sim.signals.values(1,1:end),'r')
xlabel('t')
ylabel('_Ix')
axis([0 t_final -10 10])
grid on
grid minor
subplot(3,3,2)
plot(phi_sim.time,phi_sim.signals.values(2,1:end),'b')
xlabel('t')
ylabel('_Iy')
axis([0 t_final -10 10])
grid on
grid minor
subplot(3,3,3)
plot(phi_sim.time,phi_sim.signals.values(3,1:end),'g')
xlabel('t')
ylabel('_Iz')
axis([0 t_final -10 10])
grid on
grid minor
subplot(3,3,4)
plot(E_theta_sim.time,E_theta_sim.signals.values(1,1:end),'r')
xlabel('t')
ylabel('_Eu')
axis([0 t_final -5 5])
grid on
grid minor
subplot(3,3,5)
plot(E_theta_sim.time,E_theta_sim.signals.values(2,1:end),'b')
xlabel('t')
ylabel('_Ev')
axis([0 t_final -5 5])
grid on
grid minor
subplot(3,3,6)
plot(E_theta_sim.time,E_theta_sim.signals.values(3,1:end),'g')
xlabel('t')
ylabel('_Ew')
axis([0 t_final -5 5])
grid on
grid minor
subplot(3,3,7)
plot(E_dtheta_sim.time,E_dtheta_sim.signals.values(1,1:end),'r')
xlabel('t')
ylabel('d_Eu/dt')
axis([0 t_final -3 3])
grid on
grid minor
subplot(3,3,8)
plot(E_dtheta_sim.time,E_dtheta_sim.signals.values(2,1:end),'b')
xlabel('t')
ylabel('d_Ev/dt')
axis([0 t_final -3 3])
grid on
grid minor
subplot(3,3,9)
plot(E_dtheta_sim.time,E_dtheta_sim.signals.values(3,1:end),'g')
xlabel('t')
ylabel('d_Ew/dt')
axis([0 t_final -3 3])
grid on
grid minor


%% phix, phiy, phiz and derivatives

figure('Name','Plots of phix, phiy, phiz, and derivatives')
subplot(3,3,1)
plot(phi_sim.time,phi_sim.signals.values(4,1:end),'r')
xlabel('t')
ylabel('_Iphi_x')
axis([0 t_final -2*pi 2*pi])
grid on
grid minor
set(gca,'ytick',-4*pi:pi:4*pi)
set(gca,'yticklabel',{'-4pi','-3pi','-2pi','-pi','0','pi','2pi', '3pi', '4pi'})
subplot(3,3,2)
plot(phi_sim.time,phi_sim.signals.values(5,1:end),'b')
xlabel('t')
ylabel('_Iphi_y')
axis([0 t_final -2*pi 2*pi])
grid on
grid minor
set(gca,'ytick',-4*pi:pi:4*pi)
set(gca,'yticklabel',{'-4pi','-3pi','-2pi','-pi','0','pi','2pi', '3pi', '4pi'})
subplot(3,3,3)
plot(phi_sim.time,phi_sim.signals.values(6,1:end),'g')
xlabel('t')
ylabel('_Iphi_z')
axis([0 t_final -2*pi 2*pi])
grid on
grid minor
set(gca,'ytick',-4*pi:pi:4*pi)
set(gca,'yticklabel',{'-4pi','-3pi','-2pi','-pi','0','pi','2pi', '3pi', '4pi'})
subplot(3,3,4)
plot(E_theta_sim.time,E_theta_sim.signals.values(4,1:end),'r')
xlabel('t')
ylabel('_Ep  [rad/s]')
axis([0 t_final -2*pi 2*pi])
grid on
grid minor
set(gca,'ytick',-4*pi:pi:4*pi)
set(gca,'yticklabel',{'-4pi','-3pi','-2pi','-pi','0','pi','2pi', '3pi', '4pi'})
subplot(3,3,5)
plot(E_theta_sim.time,E_theta_sim.signals.values(5,1:end),'b')
xlabel('t')
ylabel('_Eq  [rad/s]')
axis([0 t_final -2*pi 2*pi])
grid on
grid minor
set(gca,'ytick',-4*pi:pi:4*pi)
set(gca,'yticklabel',{'-4pi','-3pi','-2pi','-pi','0','pi','2pi', '3pi', '4pi'})
subplot(3,3,6)
plot(E_theta_sim.time,E_theta_sim.signals.values(6,1:end),'g')
xlabel('t')
ylabel('_Er  [rad/s]')
axis([0 t_final -2*pi 2*pi])
grid on
grid minor
set(gca,'ytick',-4*pi:pi:4*pi)
set(gca,'yticklabel',{'-4pi','-3pi','-2pi','-pi','0','pi','2pi', '3pi', '4pi'})
subplot(3,3,7)
plot(E_dtheta_sim.time,E_dtheta_sim.signals.values(4,1:end),'r')
xlabel('t')
ylabel('d_Ep/dt')
axis([0 t_final -pi pi])
grid on
grid minor
set(gca,'ytick',-2*pi:pi:2*pi)
set(gca,'yticklabel',{'-2pi','-pi','0','pi','2pi'})
subplot(3,3,8)
plot(E_dtheta_sim.time,E_dtheta_sim.signals.values(5,1:end),'b')
xlabel('t')
ylabel('d_Eq/dt')
axis([0 t_final -pi pi])
grid on
grid minor
set(gca,'ytick',-2*pi:pi:2*pi)
set(gca,'yticklabel',{'-2pi','-pi','0','pi','2pi'})
subplot(3,3,9)
plot(E_dtheta_sim.time,E_dtheta_sim.signals.values(6,1:end),'g')
xlabel('t')
ylabel('d_Er/dt')
axis([0 t_final -pi pi])
grid on
grid minor
set(gca,'ytick',-2*pi:pi:2*pi)
set(gca,'yticklabel',{'-2pi','-pi','0','pi','2pi'})

%% Thruster forces

figure('Name','Plots of thruster forces')
for i=1:8
subplot(2,4,i)
plot(F_T_sim.time,F_T_sim.signals.values(1:end,i))
xlabel('t')
ylabelstring = ['F_T [',num2str(i),']'];
ylabel(ylabelstring)
axis([0 t_final -55 55])
grid on
grid minor
end

%% tau_T

figure('Name','Plot of generalized thruster forces')
subplot(2,3,1)
plot(tau_T_sim.time,tau_T_sim.signals.values(1,1:end),'r')
xlabel('t')
ylabel('tau_T_u')
axis([0 t_final -120 120])
grid on
grid minor
subplot(2,3,2)
plot(tau_T_sim.time,tau_T_sim.signals.values(2,1:end),'b')
xlabel('t')
ylabel('tau_T_v')
axis([0 t_final -120 120])
grid on
grid minor
subplot(2,3,3)
plot(tau_T_sim.time,tau_T_sim.signals.values(3,1:end),'g')
xlabel('t')
ylabel('tau_T_w')
axis([0 t_final -120 120])
grid on
grid minor
subplot(2,3,4)
plot(tau_T_sim.time,tau_T_sim.signals.values(4,1:end),'r')
xlabel('t')
ylabel('tau_T_p')
axis([0 t_final -8 8])
grid on
grid minor
subplot(2,3,5)
plot(tau_T_sim.time,tau_T_sim.signals.values(5,1:end),'b')
xlabel('t')
ylabel('tau_T_q')
axis([0 t_final -25 25])
grid on
grid minor
subplot(2,3,6)
plot(tau_T_sim.time,tau_T_sim.signals.values(6,1:end),'g')
xlabel('t')
ylabel('tau_T_r')
axis([0 t_final -15 15])
grid on
grid minor

% Energy
figure('Name','Plot of the energy stored in the system')
plot(energy_sim.time,energy_sim.signals.values(1:end,1),'r')
hold on
plot(energy_sim.time,energy_sim.signals.values(1:end,2),'b')
hold on
plot(energy_sim.time,energy_sim.signals.values(1:end,3),'g')
xlabel('t')
ylabel('Energy [J]')
axis([0 t_final 0 20])
legend('Potential Energy','Kinetic Energy','Total Energy')
grid on
grid minor


end

