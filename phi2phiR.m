function [phiR] = phi2phiR(phi)
% Orientation: This function calculates the generalized coordinates for a given set of
% euler angles.
% Position: Remains unchanged.

% R_IE = angle2dcm(-phi(4),-phi(5),-phi(6),'XYZ');
% R_IE = eul2rotm(-phi(4:6)','ZYX');

R_IE = Eul2Rotm(phi(4:6));

phiR = [phi(1); phi(2); phi(3); R_IE(:,1); R_IE(:,2); R_IE(:,3)];

end