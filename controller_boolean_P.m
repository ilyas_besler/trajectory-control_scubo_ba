function [E_u_T] = controller_boolean_P(phiR_des,E_theta_des,E_dtheta_des,phiR,E_theta,E_dtheta)
% controller_P

R_IE = phiR2R_IE(phiR);
R_IE_des = phiR2R_IE(phiR_des);
% E_omega_IE = E_theta(4:6);
% E_omega_IE_des = E_theta_des(4:6);

e_R = 1/2*unskew(R_IE_des'*R_IE - R_IE'*R_IE_des);

e_R_norm = norm(e_R);

% e_omega = E_omega_IE - R_IE'*R_IE_des*E_omega_IE_des;
% e_omega_norm = norm(e_omega);

epsilon1 = 0.2;
% epsilon2 = 0.1;

K_P = 48;

if e_R_norm > epsilon1 %&& -e_omega_norm > epsilon2
    E_u_T_rot = -K_P*e_R/e_R_norm;
elseif e_R_norm < epsilon1 %&& -e_omega_norm ~= 0
    E_u_T_rot = -K_P*e_R/epsilon1;
else
    E_u_T_rot = [0 0 0]';
end

E_u_T = [0; 0; 0; E_u_T_rot];

end

