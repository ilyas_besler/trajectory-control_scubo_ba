function [de_I] = controller_adaptive_I(phiR_des,E_theta_des,E_dtheta_des,phiR,E_theta,E_dtheta,tau_T)
% adaptive_controller_I

R_IE = phiR2R_IE(phiR);
R_IE_des = phiR2R_IE(phiR_des);

e_R = 1/2*unskew(R_IE_des'*R_IE - R_IE'*R_IE_des);

K_I = 25;

de_I_rot = -K_I*e_R;

de_I = [0; 0; 0; de_I_rot];

end
