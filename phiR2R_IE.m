function R_IE = phiR2R_IE(phiR)
% This function calculates the rotation matrix R_IE from the elements
% contained in phiR.
R_IE = [phiR(4:6),phiR(7:9),phiR(10:12)];

end

