function [E_u_T] = controller_adaptive_D(phiR_des,E_theta_des,E_dtheta_des,phiR,E_theta,E_dtheta)
% adaptive_controller_D

R_IE = phiR2R_IE(phiR);
R_IE_des = phiR2R_IE(phiR_des);
E_omega_IE = E_theta(4:6);
E_omega_IE_des = E_theta_des(4:6);

e_omega = E_omega_IE - R_IE'*R_IE_des*E_omega_IE_des;

K_D = diag([25 50 25]); % 20 40

E_u_T_rot = -K_D*e_omega;

E_u_T = [0; 0; 0; E_u_T_rot];

end