%% Launch-file

clear variables % Don't use clear all!!! Else running a simulink model might cause a crash!
clc

fprintf('New generation and initialization of the EoM terms...\n');

run scubo_generate_kinematics.m

run scubo_generate_dynamics.m

run scubo_parameters.m

run scubo_generate_addedmassterms.m

run scubo_generate_dampingterms.m

run scubo_generate_thrusterterms.m

run scubo_initialize_EoMterms.m

fprintf('Setting initial conditions for Simulink-model... ')
phiR_IC = [0 0 0 1 0 0 0 1 0 0 0 1]';
E_theta_IC = [0 0 0 0 0 0]';
Pi_IC = [0 0 0]'; % Needed for the controller
fprintf('done!\n')
disp(' ')

fprintf('Everything done!!!\n');
disp(' ')