%% Set-up

phiR_IC = [0 0 0 1 0 0 0 1 0 0 0 1]';
E_theta_IC = [0 0 0 0 0 0]';

t_sample = 0.01;

t_final = 20 - t_sample;

t_sim = 0:t_sample:t_final;

% fw = ones(1,1/t_sample);
% zr = zeros(1,1/t_sample);

E_u_T_des_simin = 0*[t_sim; t_sim; t_sim; t_sim; t_sim; t_sim];

E_theta_des_simin = 0*[t_sim; t_sim; t_sim; t_sim; t_sim; t_sim];

E_dtheta_des_simin = 0*[t_sim; t_sim; t_sim; t_sim; t_sim; t_sim];

%% Simulation Roll

phi_des = [0 0 0 pi/4 0 0]';

phiR_des_simin = phi2phiR(phi_des)*ones(size(t_sim));

sim('scubo_model',t_sim,[],[t_sim;phiR_des_simin]',[t_sim;E_theta_des_simin]',[t_sim;E_dtheta_des_simin]',[t_sim;E_u_T_des_simin]')

scubo_plot_controller_test(phi_sim,phi_des_sim,tau_T_controller_sim)

%% Simulation Pitch

phi_des = [0 0 0 0 pi/4 0]';

phiR_des_simin = phi2phiR(phi_des)*ones(size(t_sim));

sim('scubo_model',t_sim,[],[t_sim;phiR_des_simin]',[t_sim;E_theta_des_simin]',[t_sim;E_dtheta_des_simin]',[t_sim;E_u_T_des_simin]')

scubo_plot_controller_test(phi_sim,phi_des_sim,tau_T_controller_sim)

%% Simulation Yaw

phi_des = [0 0 0 0 0 pi/4]';

phiR_des_simin = phi2phiR(phi_des)*ones(size(t_sim));

sim('scubo_model',t_sim,[],[t_sim;phiR_des_simin]',[t_sim;E_theta_des_simin]',[t_sim;E_dtheta_des_simin]',[t_sim;E_u_T_des_simin]')

scubo_plot_controller_test(phi_sim,phi_des_sim,tau_T_controller_sim)

%% Simulation of more complex Orientation-trajectory

t_sample = 0.01;

t_final = 140 - t_sample;

t_sim = 0:t_sample:t_final;

t_intervall = 0:t_sample:10 - t_sample;

phi_des_matrix = [ zeros(3,14)                                                                    ;
                   pi/6  pi/6  0     0     0     0     0     0     0     0     0     0     0     0;
                   0     0     0     pi/4 -pi/4  0     0    -pi/4 -pi/4 -pi/4 -pi/4  pi/4  pi/4  0;
                   0     0     0     0     0     0     pi/4  pi/2  pi/2  0     0    -pi/4 -pi/4  0];

phiR_des_simin = [zeros(3,size(t_sim,2));
                   ones(1,size(t_sim,2));
                  zeros(3,size(t_sim,2));
                   ones(1,size(t_sim,2));
                  zeros(3,size(t_sim,2));
                   ones(1,size(t_sim,2))];

E_u_T_des_simin = 0*[t_sim; t_sim; t_sim; t_sim; t_sim; t_sim];

E_theta_des_simin = 0*[t_sim; t_sim; t_sim; t_sim; t_sim; t_sim];

E_dtheta_des_simin = 0*[t_sim; t_sim; t_sim; t_sim; t_sim; t_sim];

for i = 1:size(phi_des_matrix,2)
    phiR_des_simin(:,size(t_intervall,2)*(i-1)+1:size(t_intervall,2)*i) = phi2phiR(phi_des_matrix(:,i))*ones(size(t_intervall));
end

sim('scubo_model',t_sim,[],[t_sim;phiR_des_simin]',[t_sim;E_theta_des_simin]',[t_sim;E_dtheta_des_simin]',[t_sim;E_u_T_des_simin]')

scubo_plot_controller_test(phi_sim,phi_des_sim,tau_T_controller_sim)
scubo_plot_adaptive_E_r_EC(E_r_EC_sim)

%% Determination of E_r_EC

t_sample = 0.01;

t_final = 150 - t_sample;

t_sim = 0:t_sample:t_final;

t_intervall = 0:t_sample:10 - t_sample;

phi_des_matrix = [ zeros(3,15)                                                                              ;
                   pi/6   pi/6   0    -pi/6  -pi/ 6  0     0     0     0     0     0     0     0     0     0;
                   0      0      0     0      0      0    -pi/4 -pi/4  0     pi/4  pi/4  0     0     0     0;
                   0      0      0     0      0      0     0     0     0     0     0     0    -pi/2 -pi/2  0];

phiR_des_simin = [zeros(3,size(t_sim,2));
                   ones(1,size(t_sim,2));
                  zeros(3,size(t_sim,2));
                   ones(1,size(t_sim,2));
                  zeros(3,size(t_sim,2));
                   ones(1,size(t_sim,2))];

E_u_T_des_simin = 0*[t_sim; t_sim; t_sim; t_sim; t_sim; t_sim];

E_theta_des_simin = 0*[t_sim; t_sim; t_sim; t_sim; t_sim; t_sim];

E_dtheta_des_simin = 0*[t_sim; t_sim; t_sim; t_sim; t_sim; t_sim];

for i = 1:size(phi_des_matrix,2)
    phiR_des_simin(:,size(t_intervall,2)*(i-1)+1:size(t_intervall,2)*i) = phi2phiR(phi_des_matrix(:,i))*ones(size(t_intervall));
end

sim('scubo_model',t_sim,[],[t_sim;phiR_des_simin]',[t_sim;E_theta_des_simin]',[t_sim;E_dtheta_des_simin]',[t_sim;E_u_T_des_simin]')

scubo_plot_controller_test(phi_sim,phi_des_sim,tau_T_controller_sim)
scubo_plot_adaptive_E_r_EC(E_r_EC_sim)

%%
% Plot the results of this simulation
% scubo_plot(phi_sim,E_theta_sim,E_dtheta_sim,F_T_sim,tau_T_sim,energy_sim)
