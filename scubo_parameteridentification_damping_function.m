function [pi_LS] = scubo_parameteridentification_damping_function(index,t_eval,...
    t_meas,E_theta_meas,F_T_meas)
% SUMMARY:
% This function calculates the linear least squares solution for the
% damping coefficients.
% It requires the functions fun_y_s and fun_h_s to have been built
% beforehand, which can be done in the file scubo_parameteridentification_damping.
% INPUTS:
% index: Integer between 1 and 6. Corresponds to the coefficients to be
%        calculated (1=u, 2=v, 3=w, 4=p, 5=q,6=r).
% t_eval: Time instances of the measurements which should be used in the
%         identification.
% t_meas, E_theta_meas, F_T_mes: Time, velocity and thruster-force measurments.
% Dimensions: t_eval=r, t_meas=m, E_theta_meas=6xm, F_T_meas=8xm.
% OUTPUT:
% pi_LS: Solution of the linear least squares problem. Dimension: pi_LS=2x1

r = length(t_eval);

E_theta = zeros(6,r);
F_T = zeros(8,r);

i = 1;
for k = 1:r
    while true
        if t_meas(i)==t_eval(k)
            break
        end
        i = i + 1;
    end
    E_theta(:,k) = E_theta_meas(1:6,i);
    F_T(:,k) = F_T_meas(1:8,i);
end

y_select = zeros(1,6);
y_select(1,index) = 1;

y_tilde = zeros(r,1);
H = zeros(r,2);
for l=1:r
    y_tilde(l) = y_select*fun_y_s(E_theta(:,l),F_T(:,l));
    H_temporary = fun_h_s(E_theta(:,l));
    H(l,:) = [H_temporary(index*2-1,index),H_temporary(index*2,index)]';
end

pi_LS = (H'*H)\(H'*y_tilde);

end

