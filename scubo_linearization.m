%% Linearization

%% Definition of equilibrium points (symbolic)

syms x_0 y_0 z_0 r11_0 r21_0 r31_0 r12_0 r22_0 r32_0 r13_0 r23_0 r33_0 'real';

phiR_0 = [x_0 y_0 z_0 r11_0 r21_0 r31_0 r12_0 r22_0 r32_0 r13_0 r23_0 r33_0]';

syms u_0 v_0 w_0 p_0 q_0 r_0 'real';

E_v_IE_0 = [u_0 v_0 w_0]';
E_omega_IE_0 = [p_0 q_0 r_0]';
E_theta_0 = [E_v_IE_0; E_omega_IE_0];

%% State-variables
% x = [delta_E_theta; delta_phiR]
% y = [E_omega_IE; R_IE_long]
% u = tau_T

delta_E_theta = E_theta - E_theta_0;
delta_phiR = phiR - phiR_0;

%% Calculate Jacobians

lin_sym.b = subs(jacobian(b,E_theta),E_theta,E_theta_0);
lin_sym.b_A = subs(jacobian(b_A,E_theta),E_theta,E_theta_0);
lin_sym.D = subs(jacobian(D,E_theta),E_theta,E_theta_0);
lin_sym.g = subs(jacobian(g,phiR),phiR,phiR_0);
lin_sym.IE_J_phiR = subs(IE_J_phiR,phiR,phiR_0);

lin_sym.IE_J_phiR_1 = subs(jacobian(IE_J_phiR*E_theta,E_theta),[phiR',E_theta'],[phiR_0',E_theta_0']);
lin_sym.IE_J_phiR_2 = subs(jacobian(IE_J_phiR*E_theta,phiR),[phiR',E_theta'],[phiR_0',E_theta_0']);

%% State-space description of system

lin_sym.A = [-inv(M + M_A)*(lin_sym.b + lin_sym.b_A + lin_sym.D), -inv(M + M_A)*lin_sym.g;
             lin_sym.IE_J_phiR_1, lin_sym.IE_J_phiR_2];

lin_sym.B = [inv(M); zeros(size(phiR,1),6)];

%%
syms x_des y_des z_des r11_des r21_des r31_des r12_des r22_des r32_des r13_des r23_des r33_des 'real';

phiR_des = [x_des y_des z_des r11_des r21_des r31_des r12_des r22_des r32_des r13_des r23_des r33_des]';

% Attitude error
R_IE_des = phiRToRIE(phiR_des);
R_error = -0.5*unskew(R_IE_des'*R_IE - R_IE'*R_IE_des); % simplify has no effect here
R_error = subs(R_error,R_IE_des,sym([1 0 0; 0 1 0; 0 0 1]));

lin_sym.R_error = subs(jacobian(R_error,[phiR]),[phiR],[phiR_0]);

%% State-space description with numerical values

lin.phiR_0 = [0 0 0 1 0 0 0 1 0 0 0 1]';
lin.E_theta_0 = [0 0 0 0 0 0]';

lin.A = subs(lin_sym.A,[phiR_0',E_theta_0'],[lin.phiR_0',lin.E_theta_0']);
lin.A = subs(lin.A,[E_r_EC,E_r_EB,E_r_ET{1}],[par.E_r_EC,par.E_r_EB,par.E_r_ET]);
lin.A = subs(lin.A,[E_Inertia,E_m_A,E_Inertia_A],[par.E_Inertia,par.E_m_A,par.E_Inertia_A]);
lin.A = subs(lin.A,[m,g_acc,F_B],[par.m,par.g_acc,par.F_B]);
lin.A = subs(lin.A,C,par.C);
lin.A = double(lin.A);

lin.B = subs(lin_sym.B,E_r_EC,par.E_r_EC);
lin.B = subs(lin.B,[E_Inertia,E_m_A,E_Inertia_A],[par.E_Inertia,par.E_m_A,par.E_Inertia_A]);
lin.B = subs(lin.B,m,par.m);
lin.B = double(lin.B);
% lin.B = lin.B(:,4:6); % For rotational movement we are only interested in the rotational part of tau_T

lin.C = double([zeros(3,3),eye(3,3),zeros(3,12);zeros(3,6),lin_sym.R_error]);

lin.D = zeros(6,6);
% lin.D = zeros(6,3); % For rotational movement we are only interested in the rotational part of tau_T

P = tf(ss(lin.A,lin.B,lin.C,lin.D));

RGA = P.*inv(P)'