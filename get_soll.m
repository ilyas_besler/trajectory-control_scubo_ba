function [p_s] = get_soll(u)

%% Input Example
%u = [p_t, p_r, [abstand 0 0 0 0 0]'] [6,3]

% Test
%u = [[0 0.1 0, 0 0 0]',[0 0 0, 0 0 0]', [0.3 0 0, 0 0 0]'];
 
%% Calculation
p_t = u(1:3,1);
p_r = u(1:3,2);
abstand = u(1,3);

v = p_t - p_r;
abs_v = sqrt(v(1,1)^2+v(2,1)^2+v(3,1)^2);
eig_v = v/abs_v;

if abs_v < abstand
   p_s = [p_r;0;0;0];
else 
    p_s = [p_t-abstand*eig_v;0;0;0];
end

end

